---
title: Welcome to my Notebook!
created: '2020-04-27T18:49:02.829Z'
modified: '2020-04-27T18:52:29.953Z'
---

# Welcome to my Notebook!
This is a repository containing my notes, all written in Github flavored markdown. These notes use the KaTeX and Ascii Math libraries to display equations, and a tagging/atttachment system that is compatable with the open source not app Notable.

You can preview these notes in Gitlabs, but the best viewing experience would come from setting up Notable on your own computer. I've included configuration instructions for Arch Linux, you should be able to figure out the setup for your environment from there. Enjoy!
