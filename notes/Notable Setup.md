---
tags: [Notebooks/Config]
title: Notable Setup
created: '2020-04-25T18:07:10.725Z'
modified: '2020-04-27T02:45:50.412Z'
---

# Notable Setup
- Clone the repo
  - `git clone https://gitlab.com/benpearo/notes.git`
- Install notable
  - `yay -S notable`
- Change notable data directory
  - <kbd>Ctrl+Shift+X</kbd>
### Git Syncronization with Gitwatch
- Install `inotify-tools` so we can watch file changes
  - `yay -S inotify-tools`
- Clone it
  - `git clone https://github.com/gitwatch/gitwatch.git`
- Install it to $PATH (/usr/bin/local)
  - `sudo install -b gitwatch.sh /usr/local/bin/gitwatch`
  - Now you can run `gitwatch` from anywhere
### Run Gitwatch on Startup (Arch)
- Search "Autostart" in programs
- Add `/usr/local/bin/gitwatch` as a program
- In preferences, check make it executable and specify arguments in the "command" section as `/usr/local/bin/gitwatch -r https://gitlab.com/benpearo/notes ~/notes`
