---
tags: [Notebooks/Config]
title: Syncthing
created: '2020-04-25T17:13:53.472Z'
modified: '2020-04-25T17:39:31.225Z'
---

# Syncthing
An encrypted, open source, peer-to-peer file syncing application. Think file syncing services like Google Drive or Dropbox, but without the server.[^Documentation]
[^Documentation]: https://docs.syncthing.net/intro/getting-started.html#

## Installation
Just use the package manager: `yay -S syncthing`

## Setup
- Start the program
  - `syncthing`
  - In the backgroud: `nohup syncthing &` [^1]
    - You will have to use `ps` and `kill` to stop it
[^1]: https://www.tecmint.com/run-linux-command-process-in-background-detach-process/

- The admin GUI starts automatically and remains available on `http://localhost:8384/`

