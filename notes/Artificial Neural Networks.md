---
attachments: [ANN_Demo.png, Furniture.png, Neuron.png, Perceptron_Network.png]
tags: [Notebooks/School/3700]
title: Artificial Neural Networks
created: '2020-04-28T04:26:53.529Z'
modified: '2020-04-28T16:25:11.587Z'
---

# Artificial Neural Networks
- We often need to classify an object relative to multiple classes
- Inspired by human brain anatomy, a network of simple units called **neurons**

![Neuron](@attachment/Neuron.png)
- The activation function could take different forms
  - Hard threshold (perceptron)
  - Logistic function (sigmoid perceptron)

## Single-Layer Feed-Forward Neural Net (Perceptron Network)
- Each node is an input variable, a neuron, or an output variable
  - Connected by directed links
- Structure is a directed acyclic graph
  - Does not apply to recurrent
- No output of any neuron is connected to input of any other neuron
  - This type can have undirected cycles, but not directed cycles (basically no loops)
  - Does not apply to multi-layered or recurrent
- A neural network may also be multi-layered or recurrent

#### Perceptron Network
![Perceptron](@attachment/Perceptron_Network.png)
- 3 input variables (1,x)
- 3 nodes (n)
- 3 output varaibles (h)
- Weights
  - Each node is associated with a weight
  - Each weight has two indices
    1. Index of neuron
    2. Index of input variable
  - Some have special values
    - $w_{12} = w_{22} = w_{32} = -1$
  - In this example, $v_1$ and $w_2$ are constants

Why are some weights constant?
- We know $w.x=w_0+w_1*x_1-x_2$
- Input to neuron $= w_0*1+w_1*x_1+w_2*x_2$
- Since $w_2=-1$, this corresponds exactly to the perceptron linear regression

### Learning Perceptron Networks
- Has $m\ge1$ neurons. Each decided whether input belongs to given class
- Each neuron can be trained by separate learning processes
  - Perceptron learning rule
  - Logistic regression

#### The Learning Process (Furniture ANN)
![Furniture](@attachment/Furniture.png)
1. Set up structure
2. Define weights
3. Pre-processing examples and neurons
4. Training neurons
5. Classifying unknown furniture
```mermaid
   graph TD;
    1((1))-->|w10|n1((n1))
    1-->|w20|n2((n2))
    1-->|w30|n3((n3))
    x1((x1))-->|w11|n1
    x1-->|w21|n2
    x1-->|w31|n3
    x2((x1))-->|12|n1
    x2-->|w22|n2
    x2-->|w32|n3
    n1-->h1
    n2-->h2
    n3-->h3

```
1. $x=(x_1,x_2)=(width,height)$
   $h_1: bed$
   $h_2: shelf$
   $h_3: table$
2. Activation for $n_1$: logistic $g(z)$
$h_j=g(w_j.x)$ $j=1,2,3$
$w_j.x=w_{j0}*1+w_{j1}*x_1+w_{j2}*x_2$
$w_{j2}=0$ so we need to find the others
3. Each example is labled with class: bed, shelf, table
    - For each example $n_j$, estimate whether $S_{wj}$ is above/below the target class $h_j$
    - $S_{wj}$ above target class: Assign $h_j$ code 1
    - $S_{wj}$ below target class: Assign $h_j$ code 0
    - $h_1$: code = 0
    - $h_2$: code = 0
    - $h_3$: code = 1
    - To train $n_j$, relabel each target example by code for $n_j$
4. ![ANN Demo](@attachment/ANN_Demo.png)
5. 
#### Classification with ANN
- We want to classify a new input $x=(x_1,x_2)$
- For each neuron $n_j$, compute $w_j.x$
  - $n_j$ with tatger code $c_j$ has a match if $c_j=1, w_j.x\ge0$ or $c_j=0, w_j.x<0$
  - Otherwise, $n_j$ is rejected

$c_j=1$ : $S_w$ above target class
$w_j.x\ge0$ : $S_w$ above input $x$
From the above, we can conclude that $x$ is in target class
