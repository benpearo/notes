---
tags: [Notebooks/Config]
title: rEFInd Configuration
created: '2020-04-25T16:22:55.184Z'
modified: '2020-04-25T17:18:29.303Z'
---

# rEFInd Configuration
This details everything required to install and customize rEFInd with a Manjaro-Windows10 dual-boot 

## Installation
- [ ] Install requirements
  - `yay -Syu refind-efi`
- [ ] Find the Windows 10 EFI directory
  - `fdisk -l`
    - Look for the block relating to Windows
  - [ ] Note the device name with type `EFI System`
    - eg. `dev/sdb2`
- [ ] Mount the EFI partition
  - `sudo mount /dev/sdb2 /boot`
- [ ] Run `refind-install`

## Customization
- [ ] Clone the repo 
  - `git clone https://github.com/EvanPurkhiser/rEFInd-minimal`
- [ ] Move the `rEFInd-minimal` folder to boot
  - `mv rEFInd-minimal /boot/EFI/Microsoft/Boot`
- [ ] Edit `refind.conf` to use the new configuration
  - `sudo nano /boot/EFI/Microsoft/Boot/refind.conf`
  - <kbd>PageDn</kbd> to the bottom
  - Add `include rEFInd-minimal/theme.conf`

## Boot
- [ ] Set your boot drive to `Windows 10 Bootloader` or whatever boots windows
- **You're Done!**

