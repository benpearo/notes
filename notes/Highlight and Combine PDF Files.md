---
tags: [Notebooks/How-To]
title: Highlight and Combine PDF Files
created: '2020-04-26T18:52:05.136Z'
modified: '2020-04-26T18:56:22.038Z'
---

# Highlight and Combine PDF Files
### Highlighting
- Open in Okular
- Press F6 to enter review mode
- Use highlight tool

### Combining
- Use pdftk
- `pdftk file1.pdf file2.pdf file3.pdf cat output output.pdf`
