---
attachments: [Applying_Linear_Solution.jpg, Gradient_Descent_Algorithm.jpg]
tags: [Notebooks/How-To, Notebooks/School/3700]
title: Regression with Linear Models
created: '2020-04-26T19:35:53.836Z'
modified: '2020-04-27T03:20:17.736Z'
---

# Regression with Linear Models
## Learning as Regression
- In learning decision trees, input vector *x* and function f(*x*) are **discrete**
- If *x* and f(*x*) are continuous, different learning methods are needed
- In this example, learn f(*x*) by examples where (*x*, f(*x*)) are age and height of a child

## Univariate Linear Regression (Lecture 3A/B)

- Has the form: &&h_w(x)=w_0+w_1x&&
- Where $w=(w_0,w_1)$ is the **weight** vector of parameters 
  - Ex $h_w(x)$ where $w=(69.0, 7.1)$
- The task to find $h_w(x)$ that best fits a training set ${(x,y)}$ of N points is called **linear regression**
- Training set $\{(x_i,y_i)\|i=1,...,N\}$, regression error is often measured bu the **squared loss function**
&&loss(h_w)=sum_(i=1)^N (y_i-h_w(x_i))^2&&
- Ex. Error of $h_w(x)$ on example $(x_i,y_i) = (5,110)$

### Analytical Solution
![Height Chart](@attachment/Regression_Height_Chart.png)
- $Loss(h_w)$ is minimized when its partial derivatives relative to $w_0$ and $w_1$ are $0$
$$\frac{\delta loss} {\delta w_0} = -2 \sum_{(i=1)}^N(y_i - h_w(x_i))$$
$$\frac {\delta loss} {\delta w_1} = -2 \sum_{(i=1)}^N(y_i - h_w(x_i))* x_i$$
- Set $\delta loss / \delta w_0 = 0$ and $\delta loss / \delta w_1 = 0$ and solve for $w$
- Let $X=\sum_{i=0}^Nx_i$, $Y=\sum_{i=0}^Ny_i$, $U=\sum_{i=0}^Nx_i^2$, $W=\sum_{i=0}^Nx_iy_i$
- Solution
  1. $w_1=(NW-XY)/(NU-X^2)$
  2. $w_0=(Y-w_1X)/N$

[Applying the Solution](@attachment/Applying_Linear_Solution.jpg)

## Multivirate Linear Regression (Lecture 4A)
Ex. Learning how colesterol and weight influenceeating habit (multiple factors)
- Let $x=(x_1,...,x_n)$ be the input vector, where $n>=1$
- An **n-variate linear function** has the form:
&&h_w(x)=w_0+w_1x+...+w_nx_n&&
where $w = (w_0,w_1,...w_n)$ is the **weight** vector of parameters
- The weight vector $w$ that minimizes squared loss can be solved analytically as in the univariate case
- *Alternative approach:* gradient descent
  1. It is also applicavle to non-linear models where no closed-form solution exists
  2. We study gradient descent for learning linear models
  
  Closed-form solution: each weight can be expressed as a function of examples (like $w_0$)

### Gradient Descent
- **Gradient** is the rate of **ascent** of a function when its variables increase in value. For univariate functions, gradient is the derivative
- For multivariate functions, gradient is a vector of partial derivatives one per variable
- Regression searches for weight vector w that minimizes loss. Thus, $w$ is modified in **descent** direction of loss function, opposite to the gradient

#### Examples
![Multivirate Chart](@attachment/Multivirate_Chart.png)
$z = f(x,y)=x^2*y^2$
$\nabla f=(\frac{\delta z} {\delta x}, \frac {\delta z} {\delta y})=(2x,2y)$

$(x,y)=(1,1)$
$\nabla f$ at $(x,y)=(1,1)$ is $(2,2)$
So at $(x,y,z)=(1,1,2)$, when $x$ grows by $\delta x$, and $y$ holds constant, $f(x,y)$ grows by $2\varDelta x$

![Linear Chart](@attachment/Linear_Thing.png)
- If gradient (slope) > 0, reduce $w$
- If gradient < 0, increase $w$
#### Algorithm
![Gradient Algo](@attachment/Multivar_Regression_Algo.png)
[Applying the Algorithm](@attachment/Gradient_Descent_Algorithm.jpg)

**Line 2:** $w = (w_0,w_1)$
**Line 6:** Gradient $=\nabla loss=(\frac{\delta loss} {\delta w_0}, \frac {\delta loss} {\delta w_1})=(d_0,d_1)$
**Line 7:** Recall $d_i$: rate of ascent
when $d_i>0$, decrease $w_i$ -> decrease loss
when $d_i<0$, increase $w_i$ -> decrease loss
so we always use negatives

**To calculate average loss per example:** $ALPE=\sqrt \frac {loss}N$
