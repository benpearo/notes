---
attachments: [Class_Threshold_Chart.png, Descent_Example.png, Descent_Steps.png, Lin_Classification_Chart.png, Log_Grad_Desc.png, Logistic_Regression_Chart.png, Perceptron_Algorithm.png, Perceptron_Learning_Rule.png, Stochastic_Descent_Algo.png]
tags: [Notebooks/School/3700]
title: Classification with Linear Models
created: '2020-04-27T03:23:00.378Z'
modified: '2020-04-28T04:23:33.107Z'
---

# Classification with Linear Models
- Let $x=(x_1,...,x_n)$ be a real input vector with class $c \isin \{0,1\}$
- Given training set $\{(x,c)\}$ of size $N$, the task of classification is to learn hypothesis $h$ for deciding the class of a new input
- Classification is often performed by sensor software
- Focuses on $n=2$
- A curve that separates the two classes is a **decision boundary**
- A linear separator is a **linear decision boundary**
- A data set that admits a linear separator is called **linearly separable (LS)**
  - You must be able to sraw a clean, straight line between the classes in a scatterplot

#### Classification Types
|                | by Decision Tree | by Linear Models |
| -------------- | ---------------- | ---------------- |
| Input vector:  | Discrete         | Continuous       |
| Mostly used by:| Agent Program    | Sensor Software  | 
| Ex.            | Wait for table   | Face recognition |

## Linear Classifier with Hard Threshold
- Let $w=(w_0,w_1)$ be a weight vector.
- For input vector $x=(x_1,x_2)$, denote $w.x=w_0+w_1x_1-x_2$
  - Vector $w$ defines a line $S_w: x_2=w_0+w_1x_1$
- A classification hypothesis takes the following form:

  &{(h_w(x)=1, if ,w.x\geq0),(h_w(x)=0, if ,w.x< 0):}&

- Hypothesis $h_w(x)$ can be viewed as a threshold function $h_w(x)=th(w.x)$
![Thresh Chart](@attachment/Class_Threshold_Chart.png)
  - Notice the below and above 0 classifications
### Example
![Lin_Chart](@attachment/Lin_Classification_Chart.png)
Ex. $w=(-3.53,1.54)$ and $x=(6.3,4.6)$

$S_w:x_2=-3.53+1.54x_1$
$w.x=-3.53+1.54*6.3-4.9=1.27$

$w.x$: how far line $S_w$ is above point $x$

$w.x>0\rarr S_w$ above &x\rarr h_w(x)=& class of &x=1& according to hypothesis
$w.x<0\rarr S_w$ below &x\rarr h_w(x)=& class of &x=0& according to hypothesis 

### Threshold Example
$h_w(x)$: $x$ is input, $w$ is parameter
$th(w.x)$: $w.x$ are combined as input

#### Applying Analytical and Gradient Solution (Lecture 4B)
- Both methods rely on $\frac {\delta loss} {\delta w_i}$ that in turn relies on $\frac {\delta h_w(x)} {\delta w_i}$
- Consider $h_w(x)$ as $th(w.x)$
- Vary $w_0$ holding $x_1w_1$ constant
- When $w.x\ne0$, $th(w.x)$ is flat so $\frac {\delta h_w(x)} {\delta w_i} = 0,\frac {\delta loss} {\delta w_i}=0$
- When $w.x=0$, $th(w.x)$ is not smooth so the derivative does not exist

We can conclude that neither of these methods will work, as we either get a zero, or no possible solution

### Solving with Perceptron Learning Rule (Lecture 5A)
![Perceptron](@attachment/Perceptron_Learning_Rule.png)
1. $w=(-3.53,1.54)$, $(x,c)=(6.3,4.9,1)$
  $w.x=w_0+w_1x_1-x_2$
  $w.x=-3.53+1.54*6.3-4.9=1.27>0$
  $h_w(x)=1$
  No change to $w$
2. $w=(-3.53,0)$, $(x,c)=(6.3,4.9,1)$
  $w.x=w_0+w_1x_1-x_2$
  $w.x=-3.53+0*6.3-4.9=-8.43<0$
  $h_w(x)=0$ so prediciton is wrong
  So continue to c) using $\alpha =0.01$
  $w_0=-3.53+0.01(1-0)=-3.52$
  $w_1=0+0.01(1-0)*6.3=0.063$
3. $w=(-3.53,3)$, $(x,c)=(6.5,7,0)$ (prediciton would be wrong, point below the line)
  $w.x=w_0+w_1x_1-x_2$
  $w.x=-3.53+3*6.5-7=8.97>0$
  $h_w(x)=1$ so prediciton is wrong
  So continue to c) using $\alpha =0.01$
  $w_0=-3.53+0.01(0-1)=-3.54$
  $w_1=3+0.01(0-1)*6.5=2.93$
  So the y-int is lowered, and slope is lowered, making it closer to the real line

#### Algorithm
![Algo](@attachment/Perceptron_Algorithm.png)
**s**: Linearly separable example set
**err**: Count of incorrewctly classified examples (by given weight factor)
**Line 9**: Case 1 from above
**Line 10**: Update using Case 2 or Case 3

- Randomly selected examples cause spikes in error

## Logistic Regression
- If data is not linearly spearable, linear classifiers with hard thresholds do not converge
- So we replace the hard threshold with a soft threshold

The **logistic function** $g(z)$ has the form:
$$g(z)=\frac {1} {1+e^{-z}}$$
- If a wrongly classified example is far from $S_w$, the error is significant
- If a wrongly classified example is close to $S_w$, the error considered insignificant

Our new hypothesis to find a weight vector $w$ that minimizes loss on a data set $\{(x,c)\}$ for hypothesis:
$$h_w(x)=g(w.x)$$

|![Logistic Regression](@attachment/Logistic_Regression_Chart.png)|$z=w.x$ increasing means $x$ is far from $S_w$<br>View $g(w.x)$ as probability that $x$ is class 1|
|-|-|
- If $x=0$, there is a 50% chance of it being either class

### Logistic Regression by Gradient Descent (Lecture 5B)
- Measure regression error by squared loss function
  &&loss(w)=sum_(x) (c-h_w(x))^2&&
- There is no closed-form sulution to find optimal $w$
- The derivative of $g(z)$ is $g'(z)=g(z)(1-g(z))$
- From $h_w(z)=g(w.x)$, we get $g'(w.x)=h_w(x)(1-h_w(x))$
- Gradient
  1. $$\frac {\delta loss} {\delta w_0}=-2\sum_x(c-h_w(x))h_w(x)(1-h_w(x))$$
  2. $$\frac {\delta loss} {\delta w_1}=-2\sum_x(c-h_w(x))h_w(x)(1-h_w(x))x_1$$

#### Example
![Gradient Descent](@attachment/Log_Grad_Desc.png)
Example with $S_w:$ $w=(-6.08, 1.8)$ and $(x,c)=((6.5,6),1)$

$w.x=-6.08+1.8*6.5-6=-0.38$
$g(w.x)=h_w(x)=\frac {1} {1+e^{0.38}}=0.406$
Chance of class 1: 0.406
Chance of class 0: 0.594
This example is nearer to class 0, but since the example is supposed to have $c=1$, it is incorrect

$\frac {\delta loss} {\delta w_0}=-2(c-h_w(x))h_w(x)(1-h_w(x))$ (no summation for single example)
$\frac {\delta loss} {\delta w_0}=-2(1-0.406)(0.406)(1-0.406)=-0.287$

$\frac {\delta loss} {\delta w_1}=-2(c-h_w(x))h_w(x)(1-h_w(x))x_1$
$\frac {\delta loss} {\delta w_1}=-0.287*6.5=-1.86$

Meaning: Increasing $w_0$ and $w_1$, reducing loss (because of negative values)

### Algorithm for Logistic Regression (Lecture 6A)
Method: Stochastic gradient descent
- We will compute gradient based on a single example, like the above example

![StoDesc](@attachment/Stochastic_Descent_Algo.png)
**Line 7**: if |v| is within threshold. ex $|v|<0.001$
**stc**: descent step count
**$ls_0$**: loss from last descent step (over all examples)
**DL**: difference in loss between adjacent steps (summed over multiple steps)
**M**: a largeish integer like 100

What is the minimal loss when the data set is linearly separable?
- We may expect $min(loss)=0$, but not always true
- When $x$ is close to $S_w$, $w.x$ is close to 0
  $\rarr h_w(x)=g(w.x)$ is close to 0.5
  $\rarr |c-h_w(x)|$ is close to 0.5
- In general, $min(loss)>0$, even if the example set is linearly separable
- We cannot terminate with the test $ls_1>0$

Can a threshold $T$ be used to control termination? Like $ls_1 < T$?
- By x, $min(loss)$ has a wide range of values
- $T$ is small, with s having many close to $S_w$ examples, descent would not be able to stop
- If $T$ is large, descent can stop before a good $S_w$ is found
- So no, we can't use $T$

Can we control termination with a threshold $T$ for the difference of loss in adjacent steps? Like $|ls_1-ls_0|<T$?
- By using random examples at each step, the adjacent steps may select similar examples
- If two values are really close, we may be less than T and terminate prematurely

#### Example Execution
![Demo](@attachment/Descent_Example.png)
- If D = 2, M = 100, average loss difference per step would be $2/100=0.02$
- Initial weight factor is $w=(-2.93, 0.24)$ which is lower than the actual and almost horizontal, which is below all examples, classifying everything incorrectly

**Step 6**
![Steps](@attachment/Descent_Steps.png)












